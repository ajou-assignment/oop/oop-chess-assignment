package Homework1;

public class Bishop  extends Piece{
    public Bishop(Board board, int team, int x, int y) {
        super(board, "B", team, x, y);
        initMoveOffsetsList();
    }

    @Override
    protected void initMoveOffsetsList() {
        moveOffsetsList = new int[][] {{0, -1}, //N
                {2, -2}, //NE
                {1, 0}, //E
                {2, 2}, //SE
                {0, 1}, //S
                {-2, 2}, //SW
                {-1, 0}, //W
                {-2, -2}}; //NW
    }
}
