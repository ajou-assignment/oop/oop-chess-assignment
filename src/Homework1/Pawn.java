package Homework1;

public class Pawn extends Piece{
    public Pawn(Board board, int team, int x, int y) {
        super(board, "P", team, x, y);
        initMoveOffsetsList();
    }

    @Override
    protected void initMoveOffsetsList() {
        moveOffsetsList = new int[][] {{0, -1}, //N
                {0, 0}, //NE
                {0, 0}, //E
                {0, 0}, //SE
                {0, 1}, //S
                {0, 0}, //SW
                {0, 0}, //W
                {0, 0}}; //NW

    }
}
