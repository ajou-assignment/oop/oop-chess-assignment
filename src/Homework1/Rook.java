package Homework1;

public class Rook extends Piece {
    public Rook(Board board, int team, int x, int y) {
        super(board, "R", team, x, y);
        initMoveOffsetsList();
    }

    @Override
    protected void initMoveOffsetsList() {
        moveOffsetsList = new int[][] {{0, -2}, //N
                {1, -1}, //NE
                {2, 0}, //E
                {1, 1}, //SE
                {0, 2}, //S
                {-1, 1}, //SW
                {-2, 0}, //W
                {-1, -1}}; //NW

    }
}
