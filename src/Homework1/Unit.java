package Homework1;

public class Unit {
    protected int x, y;

    static enum Type {
        TILE,
        PAWN,
        KNIGHT,
        ROOK,
        BISHOP,
        QUEEN,
        KING
    }

    public Unit(int x, int y) {
        setPos(x, y);
    }

    public void setPos(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
