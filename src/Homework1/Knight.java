package Homework1;

public class Knight extends Piece {

    public Knight(Board board, int team, int x, int y) {
        super(board, "N", team, x, y);
        initMoveOffsetsList();
    }

    @Override
    protected void initMoveOffsetsList() {

    }

    @Override
    public boolean isInMovingRange(int moveX, int moveY) {
        int offsetX = moveX - x; //offset to move
        int offsetY = moveY - y;
        if(Math.abs(offsetX) + Math.abs(offsetY) == 3) {
            if(!board.isEmpty(moveX, moveY)) // blocked by other piece
            {
                if(isEnemy((Piece)board.getUnit(moveX ,moveY))) //if destination, kill enemy
                    return true;
                return false;
            }

            return true;

        }
        return false;
    }
}