package Homework1;

public class Tile extends Unit{
    public Tile(int x, int y) {
        super(x, y);
    }

    public String toString() {
        if(x % 2 == y %2)
            return "1";
        return "0";
    }
}

