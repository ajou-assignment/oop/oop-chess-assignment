package Homework1;
public abstract class BoardGame {
    private Board board;

    public static class BoardNotInitializedException extends Exception {

    }


    public abstract void play();
    public abstract void printMenu();
    public abstract void menu();
    public abstract void showBoard() throws BoardNotInitializedException;

    public void end() {
        System.out.println("프로그램을 종료합니다");
        System.exit(0);
    }

}
