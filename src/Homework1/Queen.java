package Homework1;

public class Queen extends Piece {
    public Queen(Board board, int team, int x, int y) {
        super(board, "Q", team, x, y);
        initMoveOffsetsList();
    }

    @Override
    protected void initMoveOffsetsList() {
        moveOffsetsList = new int[][] {{0, -10}, //N
                {10, -10}, //NE
                {10, 0}, //E
                {10, 10}, //SE
                {0, 10}, //S
                {-10, 10}, //SW
                {-10, 0}, //W
                {-10, -10}}; //NW
    }
}
