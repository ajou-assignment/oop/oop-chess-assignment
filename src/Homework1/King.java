package Homework1;

public class King  extends Piece{
    public King(Board board, int team, int x, int y) {
        super(board, "K", team, x, y);
        initMoveOffsetsList();
    }

    @Override
    protected void initMoveOffsetsList() {
        moveOffsetsList = new int[][] {{0, -3}, //N
                {3, -3}, //NE
                {1, 0}, //E
                {1, 1}, //SE
                {0, 1}, //S
                {-1, 1}, //SW
                {-1, 0}, //W
                {-3, -3}, //NW //up

                {0, -1}, //N
                {1, -1}, //NE
                {1, 0}, //E
                {3, 3}, //SE
                {0, 3}, //S
                {-3, 3}, //SW
                {-1, 0}, //W
                {-1, -1}}; //NW //down
    }
}
