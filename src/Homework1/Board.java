package Homework1;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Board{
    private int width, height;

    private Unit[][] units;

    public Unit[][] getUnits() {
        return units;
    }

    public Unit getUnit(int x, int y) {
        return units[y][x];
    }

    public void setUnit(int x, int y, Unit unit) {
        units[y][x] = unit;
        unit.setPos(x, y);
    }

    public Board(int width, int height,  boolean random) {
        this.width = width;
        this.height = height;
        initPieces(random);
    }

    public boolean isInBoard(int x, int y) {
        return 0 <= x && x <= width && 0 <= y && y <= height;
    }

    public boolean isEmpty(int x, int y) {
        if(!isInBoard(x, y))
            return false;
        return units[y][x] instanceof Tile;
    }
    public Unit createUnit(Unit.Type unitType, int team, int x, int y) { //Polymorph
        if(unitType == Unit.Type.PAWN)
            return new Pawn(this, team, x, y);
        else if(unitType == Unit.Type.KNIGHT)
            return new Knight(this, team, x, y);
        else if(unitType == Unit.Type.ROOK)
            return new Rook(this, team, x, y);
        else if(unitType == Unit.Type.BISHOP)
            return new Bishop(this, team, x, y);
        else if(unitType == Unit.Type.QUEEN)
            return new Queen(this, team, x, y);
        else if(unitType == Unit.Type.KING)
            return new King(this, team, x, y);
        else
            return new Tile(x, y);
    }

    public boolean deletePiece(int x, int y) {
        if(isEmpty(x, y))
            return false;
        setUnit(x, y, createUnit(Unit.Type.TILE,  -1, x, y)); //warning : memory leak  but garbage collected.
        return true;
    }

    private void initPieces(boolean random) {
        units = new Unit[height][width];
        Unit.Type[][] unitTypes = {
                {Unit.Type.PAWN, Unit.Type.ROOK, Unit.Type.KNIGHT, Unit.Type.BISHOP, Unit.Type.KING,
                 Unit.Type.QUEEN, Unit.Type.BISHOP, Unit.Type.KNIGHT, Unit.Type.ROOK, Unit.Type.PAWN},
                {Unit.Type.PAWN, Unit.Type.PAWN, Unit.Type.PAWN, Unit.Type.PAWN, Unit.Type.PAWN,
                 Unit.Type.PAWN, Unit.Type.PAWN, Unit.Type.PAWN, Unit.Type.PAWN, Unit.Type.PAWN, },
                {Unit.Type.TILE, Unit.Type.TILE, Unit.Type.TILE, Unit.Type.TILE, Unit.Type.TILE,
                        Unit.Type.TILE, Unit.Type.TILE, Unit.Type.TILE, Unit.Type.TILE, Unit.Type.TILE, },
                {Unit.Type.TILE, Unit.Type.TILE, Unit.Type.TILE, Unit.Type.TILE, Unit.Type.TILE,
                        Unit.Type.TILE, Unit.Type.TILE, Unit.Type.TILE, Unit.Type.TILE, Unit.Type.TILE, },
                {Unit.Type.TILE, Unit.Type.TILE, Unit.Type.TILE, Unit.Type.TILE, Unit.Type.TILE,
                        Unit.Type.TILE, Unit.Type.TILE, Unit.Type.TILE, Unit.Type.TILE, Unit.Type.TILE, },
                {Unit.Type.TILE, Unit.Type.TILE, Unit.Type.TILE, Unit.Type.TILE, Unit.Type.TILE,
                        Unit.Type.TILE, Unit.Type.TILE, Unit.Type.TILE, Unit.Type.TILE, Unit.Type.TILE, },
                {Unit.Type.TILE, Unit.Type.TILE, Unit.Type.TILE, Unit.Type.TILE, Unit.Type.TILE,
                        Unit.Type.TILE, Unit.Type.TILE, Unit.Type.TILE, Unit.Type.TILE, Unit.Type.TILE, },
                {Unit.Type.TILE, Unit.Type.TILE, Unit.Type.TILE, Unit.Type.TILE, Unit.Type.TILE,
                        Unit.Type.TILE, Unit.Type.TILE, Unit.Type.TILE, Unit.Type.TILE, Unit.Type.TILE, },
                {Unit.Type.PAWN, Unit.Type.PAWN, Unit.Type.PAWN, Unit.Type.PAWN, Unit.Type.PAWN,
                        Unit.Type.PAWN, Unit.Type.PAWN, Unit.Type.PAWN, Unit.Type.PAWN, Unit.Type.PAWN, },
                {Unit.Type.PAWN, Unit.Type.ROOK, Unit.Type.KNIGHT, Unit.Type.BISHOP, Unit.Type.KING,
                 Unit.Type.QUEEN, Unit.Type.BISHOP, Unit.Type.KNIGHT, Unit.Type.ROOK, Unit.Type.PAWN}, };


       for(int y = 0; y < height; y++) {
           for(int x = 0; x < width; x++) {
               int team = y < height / 2 ? 1 : 0;
               setUnit(x, y, createUnit(unitTypes[y][x], team, x, y));
           }
       }

        if(random) {
            List<Integer> indices = IntStream.range(0, width * height).boxed().collect(Collectors.toList());
            Collections.shuffle(indices);

            //backup
            Unit[][] units = this.units.clone();
            for(int i = 0; i < units.length; i++)
                units[i] = this.units[i].clone();

            for(int i = 0; i < indices.size(); i++) {
                int idx = indices.get(i);
                int toY = i / units.length;
                int toX = i % units[0].length;
                int fromY = idx / this.units.length;
                int fromX = idx % this.units[0].length;

                Unit unit = getUnit(fromX, fromY); //polymorph
                unit.setPos(toX, toY);
                units[toY][toX] = unit;
            }

            this.units = units;
        }
    }

    public boolean movePiece(int fromX, int fromY,  int toX, int toY) {
        Piece piece = (Piece)getUnit(fromX, fromY);
        if(!piece.isInMovingRange(toX, toY)) //restrict moving range
            return false;

        /*
        if(!isEmpty(toX, toY)) { // if exsits piece
            Piece toPiece = (Piece)getUnit(toX, toX);
            if(!piece.isEnemy(toPiece)) //if my team piece, do not delete
                return false;
        }
        */
        setUnit(toX, toY, piece);
        Tile tile = (Tile)createUnit(Unit.Type.TILE, -1, fromX, fromY);
        setUnit(fromX, fromY, tile);
        return true;

    }


}
