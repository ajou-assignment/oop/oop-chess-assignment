package Homework1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ChessGame extends BoardGame {

    Board board;
    Scanner scanner;

    final private int WIDTH = 10;
    final private int HEIGHT = 10;


    public ChessGame() {
        scanner = new Scanner(System.in);
    }

    private enum Action{
        EMPTY, //0
        CREATE_PIECE,
        CREATE_PIECE_RANDOMLY,
        DELETE_PIECE,
        SHOW_BOARD,
        MOVE_PIECE,
        END,
    }

    public int[] strToPos(String str) {

        int x = str.charAt(0) - 'A';

        int y = HEIGHT - Integer.parseInt(str.substring(1));
        int[] pos = {x, y};
        return pos;
    }

    public void play() {
        while(true) {
            printMenu();
            menu();
            System.out.println("\n========================");
        }
    }


    @Override
    public void printMenu() {
        System.out.println("원하는 메뉴를 선택하세요.");
        System.out.println("1. 체스 말 생성");
        System.out.println("2. 체스 말 랜덤 생성");
        System.out.println("3. 체스 말 삭제");
        System.out.println("4. 체스판 보기");
        System.out.println("5. 체스 말 이동");
        System.out.println("6. 종료");
        System.out.printf(">> ");
    }

    public boolean hasBoard() { //pieces is created?
        return board != null;
    }

    @Override
    public void menu() {
        int input = -1;
        try {
            input = scanner.nextInt(); scanner.nextLine();
            if (input == Action.CREATE_PIECE.ordinal())
                createPieces(false);
            else if (input == Action.CREATE_PIECE_RANDOMLY.ordinal())
                createPieces(true);
            else if (input == Action.DELETE_PIECE.ordinal())
                deletePiece();
            else if (input == Action.SHOW_BOARD.ordinal())
                showBoard();
            else if (input == Action.MOVE_PIECE.ordinal())
                movePiece();
            else if (input == Action.END.ordinal())
                end();
            else
                throw new InputMismatchException();
        }
        catch(BoardNotInitializedException e) {
            System.out.println("체스판을 생성해야합니다.");
        }
        catch(InputMismatchException e) {
            System.out.println("올바르게 입력해주세요.");
        }
        catch(Piece.PieceNotFoundException e) {
            System.out.println("해당 위치에 체스 말이 없습니다.");
        }
        catch(Exception e) {
            System.out.println("오류가 발생했습니다.");
        }
        finally {
            if(input == -1)
                scanner.nextLine();
        }
    }

    public void deletePiece() throws Piece.PieceNotFoundException, BoardNotInitializedException{
        if(!hasBoard())
            throw new BoardNotInitializedException();

        System.out.printf("삭제를 원하는 체스 말의 위치를 입력하세요. ");
        String sPos;
        int[] pos;
        int x, y;

        try {
            sPos = scanner.nextLine();
            pos = strToPos(sPos);
            x = pos[0];
            y = pos[1];
        }
        catch(Exception e) {
            throw new InputMismatchException();
        }

        if (board.isInBoard(x, y)) {
            if (board.deletePiece(x, y))
                System.out.println("체스 말이 삭제되었습니다.");
            else
                throw new Piece.PieceNotFoundException();
        }
        else
            throw new InputMismatchException();
    }

    public void createPieces(boolean random) {
        if(hasBoard()) {
            System.out.println("체스판이 이미 생성되어있습니다.");
            return;
        }
        try {
            board = new Board(WIDTH, HEIGHT, random);
            System.out.println("체스판 생성이 완료되었습니다.");
        }
        catch(Exception e) {
            System.out.println("체스판 생성중 오류가 발생하였습니다.");
        }
    }


    @Override
    public  void showBoard() throws BoardNotInitializedException {
        if(!hasBoard())
            throw new BoardNotInitializedException();
        Unit[][] units = board.getUnits();
        for(int y = 0; y < HEIGHT; y++) {
            for(int x = 0; x < WIDTH; x++) {
                System.out.printf(String.format("%-2s ", units[y][x].toString()));
            }
            System.out.println("");
        }

    }

    public  void movePiece() throws BoardNotInitializedException, Piece.PieceNotFoundException {
        if(!hasBoard())
            throw new BoardNotInitializedException();
        String sFromPos, sToPos;
        int[] fromPos, toPos;
        int fromX, fromY, toX, toY;

        try {
            System.out.printf("움직일 말의 위치를 입력하세요. ");
            sFromPos = scanner.nextLine();

            System.out.printf("말이 이동할 위치를 입력하세요. ");
            sToPos = scanner.nextLine();
            if(!(sFromPos.length() <= 3 || sToPos.length() <= 3))
                throw new InputMismatchException();

            fromPos = strToPos(sFromPos);
            fromX = fromPos[0];
            fromY = fromPos[1];
            toPos = strToPos(sToPos);
            toX = toPos[0];
            toY = toPos[1];
        }
        catch(Exception e) {
            throw new InputMismatchException();
        }

        if(!(board.isInBoard(fromX, fromY) && board.isInBoard(toX, toY))) //restrict board range
            throw new InputMismatchException();

        if(board.isEmpty(fromX, fromY))
            throw new Piece.PieceNotFoundException();

        if(!board.movePiece(fromX, fromY, toX, toY)){
            System.out.println("해당 위치로 이동할 수 없습니다.");
            return;
        }
        System.out.printf("말을 %s(으)로 이동 시켰습니다.\n", sToPos);
    }


}
