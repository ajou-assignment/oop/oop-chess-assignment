package Homework1;

public abstract class Piece extends Unit {

    enum Dirs
    {
        N,
        NE,
        E,
        SE,
        S,
        SW,
        W,
        NW,
    }

    final private int[][] DIR_OFFSETS = {{0, -1},    //N
                                {1, -1},    //NE
                                {1, 0},    //E
                                {1, 1},   //SE
                                {0, 1},   //S
                                {-1, 1},  //SW
                                {-1, 0},   //W
                                {-1, -1}};  //NW

    protected int[][] moveOffsetsList;
    private int team;
    private String name;
    protected Board board;

    protected abstract void initMoveOffsetsList();

    public Piece(Board board, String name, int team, int x, int y) {
        super(x, y);
        this.board = board;
        this.team = team;
        this.name = name;

    }

    public int getTeam() {
        return team;
    }

    public static class PieceNotFoundException extends Exception {

    }

    public boolean isEnemy(Piece piece)
    {
        return team != piece.getTeam();

    }

    private int[] convertUnitVector(int x, int y) {
        int[] unitVector = {0, 0};
        unitVector[0] = x == 0 ? 0 : x / Math.abs(x);
        unitVector[1] = y == 0 ? 0 : y / Math.abs(y);
        return unitVector;
    }

    public boolean isInMovingRange(int moveX, int moveY) {
        int offsetX = moveX - this.x; //offset to move
        int offsetY = moveY - this.y;

        int move_dir = -1;

        int[] moveOffsets = {0, 0};

        //find direction
        for(Dirs enum_dir: Dirs.values()) {
            int dir = enum_dir.ordinal();
            moveOffsets = moveOffsetsList[dir];
            if(moveOffsetsList.length > Dirs.values().length) //if various offsets
                if (team == 1) //change direction with team
                    moveOffsets = moveOffsetsList[dir + Dirs.values().length];

            int[] pieceUnitVector = convertUnitVector(offsetX, offsetY);
            int[] moveUnitVector = convertUnitVector(moveOffsets[0], moveOffsets[1]);

            if(moveUnitVector[0] == pieceUnitVector[0] &&
                    moveUnitVector[1] == pieceUnitVector[1])  {
                move_dir = dir;
                break;
            }
        }
        if(move_dir == -1) //direction not found
            return false;

        int[] dirOffsets = DIR_OFFSETS[move_dir];

        int checkOffsetX = 0;
        int checkOffsetY = 0;
        int movableDistance = (int)Math.sqrt(Math.pow(moveOffsets[0], 2) + Math.pow(moveOffsets[1], 2));
        for(int i = 0; i < movableDistance; i++) {
            checkOffsetX += dirOffsets[0];
            checkOffsetY += dirOffsets[1];
            int x = this.x + checkOffsetX;
            int y = this.y + checkOffsetY;
            if(!board.isEmpty(x, y)) {// blocked by other piece

                if(x == moveX && y == moveY &&
                        isEnemy((Piece)board.getUnit(x ,y))) //if destination, kill enemy
                    return true;

                return false;
            }
            if(checkOffsetX == offsetX &&
                checkOffsetY == offsetY )
                return true;
        }

        return false;
    }

    @Override
    public String toString() {
      return name + team;
    }
}

/*

8 1


 */